import { IsNotEmpty, Length } from 'class-validator';
import { Matches } from 'class-validator/types/decorator/decorators';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 16)
  login: string;

  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @Matches(/^(?=.*\d)(?=.*[!@#$%&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  password: string;
}
